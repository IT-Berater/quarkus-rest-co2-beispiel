package de.wenzlaff;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Beispiel Test.
 * 
 * @author Thomas Wenzlaff
 *
 */
@QuarkusTest
public class InfoResourceTest {

	@Test
	public void testgGtCo2WertEndpoint() {
		given().when().get("/info").then().statusCode(200).body(is("CO2 Wert: 404 ppm"));
	}
}