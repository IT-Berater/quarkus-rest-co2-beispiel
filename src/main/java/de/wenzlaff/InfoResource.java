package de.wenzlaff;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/info")
public class InfoResource {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getCo2Wert() {
		String wert = "404 ppm";
		return "CO2 Wert: " + wert;
	}
}
